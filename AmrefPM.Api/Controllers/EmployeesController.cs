﻿using AmrefPM.DataAccess.Entities;
using AmrefPM.DataAccess.Repositories;
using AmrefPM.DataAccess.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq;

namespace AmrefPM.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : AmrefBaseController<Employee, EmployeeRepository>
    {
        private readonly EmployeeRepository _repository;
        public EmployeesController(EmployeeRepository repository) : base(repository)
        {
            _repository = repository;
        }

        [HttpGet("projectManagers")]
        public async Task<ActionResult<IEnumerable<Employee>>> GetProjectManagers()
        {
            var items = await _repository.GetProjectManagers();
            return Ok(items);
        }
    }
}
