﻿using AmrefPM.DataAccess.Entities;
using AmrefPM.DataAccess.Repositories;
using AmrefPM.DataAccess.UnitOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmrefPM.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KpisController : AmrefBaseController<Kpi, KpiRepository>
    {
        public KpisController(KpiRepository repository) : base(repository)
        {

        }
    }
}
