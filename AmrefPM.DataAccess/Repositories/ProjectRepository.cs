﻿using AmrefPM.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace AmrefPM.DataAccess.Repositories
{
    public class ProjectRepository: BaseRepository<Project, AmrefContext>, IProjectRepository
    {
        public ProjectRepository(AmrefContext context) : base(context)
        {
        }
    }
}
