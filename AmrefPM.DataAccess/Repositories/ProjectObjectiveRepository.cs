﻿using AmrefPM.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace AmrefPM.DataAccess.Repositories
{
    public class ProjectObjectiveRepository : BaseRepository<ProjectObjective, AmrefContext>, IProjectObjectiveRepository
    {
        public ProjectObjectiveRepository(AmrefContext context) : base(context)
        {
        }
    }
}
