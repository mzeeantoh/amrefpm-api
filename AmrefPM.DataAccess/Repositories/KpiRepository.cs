﻿using AmrefPM.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmrefPM.DataAccess.Repositories
{
    public class KpiRepository: BaseRepository<Kpi, AmrefContext>, IKpiRepository
    {
        public KpiRepository(AmrefContext context) : base(context)
        {
        }
    }
}
