﻿using AmrefPM.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AmrefPM.DataAccess.Repositories
{
    public class BaseRepository<T, TContext> : IBaseRepository<T>
        where T : class, IEntity
        where TContext : DbContext
    {
        protected readonly DbContext _context;
        public BaseRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression);
        }
        public IQueryable<T> GetAll()
        {
            return _context.Set<T>();
        }
        public async Task<T> GetById(int id)
        {
            var item = await _context.Set<T>().FindAsync(id);
            return item;
        }


        public async Task<T> AddAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task AddRange(IEnumerable<T> entities)
        {
            if (!entities.Any())
            {
                throw new ArgumentNullException($"{nameof(AddRange)} must contain one or more entities");
            }

            await _context.Set<T>().AddRangeAsync(entities);
        }

        public async Task<T> Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(Update)} must contain one or more entities");
            }

            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return entity;
        }


        public async Task<int> Remove(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(Remove)} entity must not be null");
            }

            _context.Set<T>().Remove(entity);
            int rowsAffected = await _context.SaveChangesAsync();

            return rowsAffected;
        }

        public async Task<T> Delete(int id)
        {
            var entity = await _context.Set<T>().FindAsync(id);
            if (entity == null)
            {
                return entity;
            }

            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task<int> RemoveRange(IEnumerable<T> entities)
        {
            if (!entities.Any())
            {
                throw new ArgumentNullException($"{nameof(RemoveRange)} must contain one or more entities");
            }

            _context.Set<T>().RemoveRange(entities);

            int rowsAffected = await _context.SaveChangesAsync();

            return rowsAffected;
        }
    }
}
