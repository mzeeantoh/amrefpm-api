﻿using AmrefPM.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AmrefPM.DataAccess.Repositories
{
    public class EmployeeRepository: BaseRepository<Employee, AmrefContext>, IEmployeeRepository
    {
        private readonly AmrefContext _context;
        public EmployeeRepository(AmrefContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Employee>> GetProjectManagers()
        {
            var employees = this.Find(emp => emp.DesignationNavigation.Title.ToLower() == "project manager");

            return await employees.ToListAsync();
        }
    }
}
