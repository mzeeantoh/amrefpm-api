﻿using AmrefPM.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AmrefPM.DataAccess.Repositories
{
    public interface IBaseRepository<T>
        where T : class, IEntity
    {
        Task<T> GetById(int id);

        IQueryable<T> GetAll();

        IQueryable<T> Find(Expression<Func<T, bool>> expression);

        Task<T> AddAsync(T entity);

        Task AddRange(IEnumerable<T> entities);

        Task<int> Remove(T entity);

        Task<int> RemoveRange(IEnumerable<T> entities);

        Task<T> Delete(int id);
        Task<T> Update(T entity);
    }
}
