﻿using AmrefPM.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace AmrefPM.DataAccess.Repositories
{
    public class DesignationRepository : BaseRepository<Designation, AmrefContext>, IDesignationRepository
    {
        public DesignationRepository(AmrefContext context) : base(context)
        {
        }
    }
}
