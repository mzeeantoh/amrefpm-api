﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace AmrefPM.DataAccess
{
   
    public static class Extensions
    {
        public static IServiceCollection AddDbContext<TContext>(this IServiceCollection services, IConfiguration Configuration, string connectionString) where TContext : DbContext
        {
            services.AddDbContext<TContext>(options => options.UseSqlServer(Configuration.GetConnectionString(connectionString)));
            return services;
        }
    }
}
