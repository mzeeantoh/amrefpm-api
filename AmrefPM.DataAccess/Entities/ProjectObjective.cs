﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AmrefPM.DataAccess.Entities
{
    public partial class ProjectObjective : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Kpi { get; set; }
        public short Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? ProjectId { get; set; }

        public virtual Kpi Project { get; set; }
        public virtual Project ProjectNavigation { get; set; }
    }
}
