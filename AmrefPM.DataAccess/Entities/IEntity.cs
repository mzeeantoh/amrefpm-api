﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AmrefPM.DataAccess.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
