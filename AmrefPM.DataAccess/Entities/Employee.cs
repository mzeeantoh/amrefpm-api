﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AmrefPM.DataAccess.Entities
{
    public partial class Employee : IEntity
    {
        public Employee()
        {
            Projects = new HashSet<Project>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string OtherName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int Designation { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public virtual Designation DesignationNavigation { get; set; }
        public virtual ICollection<Project> Projects { get; set; }
    }
}
