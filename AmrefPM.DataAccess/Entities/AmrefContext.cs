﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace AmrefPM.DataAccess.Entities
{
    public partial class AmrefContext : DbContext
    {
        public AmrefContext()
        {
        }

        public AmrefContext(DbContextOptions<AmrefContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Designation> Designations { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Kpi> Kpis { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectObjective> ProjectObjectives { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-7V137E4;Initial Catalog=Amrefc;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Designation>(entity =>
            {
                entity.Property(e => e.Title)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasIndex(e => e.Email, "Email")
                    .IsUnique();

                entity.HasIndex(e => e.PhoneNumber, "PhoneNumber")
                    .IsUnique();

                entity.Property(e => e.CreatedAt).HasPrecision(0);

                entity.Property(e => e.DeletedAt).HasPrecision(0);

                entity.Property(e => e.Designation).HasDefaultValueSql("('0')");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OtherName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasPrecision(0);

                entity.HasOne(d => d.DesignationNavigation)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.Designation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_employee_designation");
            });

            modelBuilder.Entity<Kpi>(entity =>
            {
                entity.Property(e => e.CreatedAt).HasPrecision(0);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasPrecision(0);
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.HasIndex(e => e.ProjectManager, "FK_Projects_Employees");

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasPrecision(0);

                entity.Property(e => e.DeletedAt).HasPrecision(0);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasPrecision(0);

                entity.HasOne(d => d.ProjectManagerNavigation)
                    .WithMany(p => p.Projects)
                    .HasForeignKey(d => d.ProjectManager)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Projects_Employees");
            });

            modelBuilder.Entity<ProjectObjective>(entity =>
            {
                entity.HasIndex(e => e.Kpi, "FK_projectobjectives_Kpi_Idx");

                entity.Property(e => e.CreatedAt).HasPrecision(0);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasPrecision(0);

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.ProjectObjectives)
                    .HasForeignKey(d => d.ProjectId)
                    .HasConstraintName("FK_projectobjectives_Kpi");

                entity.HasOne(d => d.ProjectNavigation)
                    .WithMany(p => p.ProjectObjectives)
                    .HasForeignKey(d => d.ProjectId)
                    .HasConstraintName("FK_ProjectObjectives_Projects");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
