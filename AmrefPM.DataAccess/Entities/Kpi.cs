﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AmrefPM.DataAccess.Entities
{
    public partial class Kpi : IEntity
    {
        public Kpi()
        {
            ProjectObjectives = new HashSet<ProjectObjective>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public virtual ICollection<ProjectObjective> ProjectObjectives { get; set; }
    }
}
