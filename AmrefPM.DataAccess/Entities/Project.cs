﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AmrefPM.DataAccess.Entities
{
    public partial class Project : IEntity
    {
        public Project()
        {
            ProjectObjectives = new HashSet<ProjectObjective>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public double Budget { get; set; }
        public string Country { get; set; }
        public short CountryCode { get; set; }
        public int ProjectManager { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }

        public virtual Employee ProjectManagerNavigation { get; set; }
        public virtual ICollection<ProjectObjective> ProjectObjectives { get; set; }
    }
}
