﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AmrefPM.DataAccess.Entities
{
    public partial class Designation : IEntity
    {
        public Designation()
        {
            Employees = new HashSet<Employee>();
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
