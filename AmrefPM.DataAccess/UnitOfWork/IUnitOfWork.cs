﻿using AmrefPM.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace AmrefPM.DataAccess.UnitOfWork
{
    public interface IUnitOfWork:IDisposable
    {
        IKpiRepository Kip { get; }
        IEmployeeRepository Employee { get; }
        IDesignationRepository Designation { get; }
        IProjectRepository Project { get; }
        IProjectObjectiveRepository ProjectObjective { get; }
        int Complete();
    }

}
