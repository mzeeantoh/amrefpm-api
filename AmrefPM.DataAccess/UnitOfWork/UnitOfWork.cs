﻿using AmrefPM.DataAccess.Entities;
using AmrefPM.DataAccess.Repositories;

namespace AmrefPM.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AmrefContext _context;
        public UnitOfWork(AmrefContext context,
            IDesignationRepository designationRepository,
            IKpiRepository kpiRepository,
            IEmployeeRepository employeeRepository,
            IProjectRepository projectRepository,
            IProjectObjectiveRepository projectObjectiveRepository
            )
        {
            _context = context;
            Kip = kpiRepository;
            Employee = employeeRepository;
            Designation = designationRepository;
            Project = projectRepository;
            ProjectObjective = projectObjectiveRepository;
        }

        public IKpiRepository Kip { get; private set; }

        public IEmployeeRepository Employee { get; private set; }

        public IDesignationRepository Designation { get; private set; }

        public IProjectRepository Project { get; private set; }

        public IProjectObjectiveRepository ProjectObjective { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }

}
